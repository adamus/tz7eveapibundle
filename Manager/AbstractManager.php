<?php

/*
 * This file is part of the Tz7\EveApiBundle package.
 *
 * (c) Adamus TorK <https://bitbucket.org/adamus/>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Tz7\EveApiBundle\Manager;


use Doctrine\ORM\EntityManager;


class AbstractManager implements AbstractManagerInterface
{
    /** @var EntityManager */
    protected $entityManager;

    /** @var array */
    protected $map = [];

    /**
     * @param EntityManager $entityManager
     * @param array         $map
     */
    public function __construct(EntityManager $entityManager, array $map = [])
    {
        $this->entityManager = $entityManager;
        $this->map           = $map;
    }

    /**
     * @inheritdoc
     */
    public function find($model, $id)
    {
        return $this->entityManager->find($model, $id);
    }

    /**
     * @inheritdoc
     */
    public function findBy($model, array $criteria = [])
    {
        return $this->entityManager->getRepository($model)->findBy($criteria);
    }

    /**
     * @inheritdoc
     */
    public function findOneBy($model, array $criteria = [])
    {
        return $this->entityManager->getRepository($model)->findOneBy($criteria);
    }

    /**
     * @inheritdoc
     */
    public function persist($object)
    {
        $this->entityManager->persist($object);
    }

    /**
     * @inheritdoc
     */
    public function detach($object)
    {
        $this->entityManager->detach($object);
    }

    /**
     * @inheritdoc
     */
    public function remove($object)
    {
        $this->entityManager->remove($object);
    }

    /**
     * @inheritdoc
     */
    public function flush()
    {
        $this->entityManager->flush();
    }

    /**
     * @inheritdoc
     */
    public function create($model, $id)
    {
        if (isset($this->map[$model]))
        {
            $object = new $this->map[$model]($id);

            return $object;
        }
        else
        {
            throw new \InvalidArgumentException(sprintf('"%s" is not mapped', $model));
        }
    }

    /**
     * @inheritdoc
     */
    public function findOrCreate($model, $id)
    {
        return $this->find($model, $id) ?: $this->create($model, $id);
    }
}
