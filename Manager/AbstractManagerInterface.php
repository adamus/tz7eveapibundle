<?php

/*
 * This file is part of the Tz7\EveApiBundle package.
 *
 * (c) Adamus TorK <https://bitbucket.org/adamus/>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Tz7\EveApiBundle\Manager;


interface AbstractManagerInterface
{
    /**
     * @param string  $model
     * @param integer $id
     *
     * @return null|object
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     * @throws \Doctrine\ORM\TransactionRequiredException
     */
    public function find($model, $id);

    /**
     * @param string $model
     * @param array  $criteria
     *
     * @return array
     */
    public function findBy($model, array $criteria = []);

    /**
     * @param string $model
     * @param array  $criteria
     *
     * @return null|object
     */
    public function findOneBy($model, array $criteria = []);

    /**
     * @param $object
     */
    public function persist($object);

    /**
     * @param $object
     */
    public function detach($object);

    /**
     * @param $object
     */
    public function remove($object);

    /**
     */
    public function flush();

    /**
     * @param string  $model
     * @param integer $id
     *
     * @return mixed
     */
    public function create($model, $id);

    /**
     * @param string  $model
     * @param integer $id
     *
     * @return mixed
     */
    public function findOrCreate($model, $id);
}
