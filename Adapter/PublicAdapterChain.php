<?php

/*
 * This file is part of the Tz7\EveApiBundle package.
 *
 * (c) Adamus TorK <https://bitbucket.org/adamus/>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Tz7\EveApiBundle\Adapter;


use Psr\Log\LoggerInterface;
use Tz7\EveApiClient\Adapter\PublicFunctionAdapterInterface;
use Tz7\EveApiClient\Exception\ApiResultException;


class PublicAdapterChain implements PublicFunctionAdapterInterface
{
    /** @var LoggerInterface */
    private $logger;

    /** @var PublicFunctionAdapterInterface[] */
    private $adapters = [];

    /**
     * @param LoggerInterface $logger
     */
    public function __construct(LoggerInterface $logger)
    {
        $this->logger = $logger;
    }

    /**
     * @return PublicFunctionAdapterInterface[]
     */
    public function getAdapters()
    {
        return $this->adapters;
    }

    /**
     * @param PublicFunctionAdapterInterface $adapter
     *
     * @return $this
     */
    public function addAdapter(PublicFunctionAdapterInterface $adapter)
    {
        $this->adapters[] = $adapter;

        return $this;
    }

    /**
     * @inheritdoc
     */
    public function getEntityName($id)
    {
        foreach ($this->getAdapters() as $adapter)
        {
            try
            {
                return $adapter->getEntityName($id);
            }
            catch (ApiResultException $apiResultException)
            {
                $this->logException($apiResultException);
            }
        }

        throw new ApiResultException('None of the adapters in chains were able to process the call.');
    }

    /**
     * @inheritdoc
     */
    public function getEntityId($name)
    {
        foreach ($this->getAdapters() as $adapter)
        {
            try
            {
                return $adapter->getEntityId($name);
            }
            catch (ApiResultException $apiResultException)
            {
                $this->logException($apiResultException);
            }
        }

        throw new ApiResultException('None of the adapters in chains were able to process the call.');
    }

    /**
     * @inheritdoc
     */
    public function getItemName($id)
    {
        foreach ($this->getAdapters() as $adapter)
        {
            try
            {
                return $adapter->getItemName($id);
            }
            catch (ApiResultException $apiResultException)
            {
                $this->logException($apiResultException);
            }
        }

        throw new ApiResultException('None of the adapters in chains were able to process the call.');
    }

    /**
     * @inheritdoc
     */
    public function getItemId($name)
    {
        foreach ($this->getAdapters() as $adapter)
        {
            try
            {
                return $adapter->getItemId($name);
            }
            catch (ApiResultException $apiResultException)
            {
                $this->logException($apiResultException);
            }
        }

        throw new ApiResultException('None of the adapters in chains were able to process the call.');
    }

    /**
     * @inheritdoc
     */
    public function getCharacterPublicInfo($characterId)
    {
        foreach ($this->getAdapters() as $adapter)
        {
            try
            {
                return $adapter->getCharacterPublicInfo($characterId);
            }
            catch (ApiResultException $apiResultException)
            {
                $this->logException($apiResultException);
            }
        }

        throw new ApiResultException('None of the adapters in chains were able to process the call.');
    }

    /**
     * @inheritdoc
     */
    public function getCorporationPublicInfo($corporationId)
    {
        foreach ($this->getAdapters() as $adapter)
        {
            try
            {
                return $adapter->getCorporationPublicInfo($corporationId);
            }
            catch (ApiResultException $apiResultException)
            {
                $this->logException($apiResultException);
            }
        }

        throw new ApiResultException('None of the adapters in chains were able to process the call.');
    }

    /**
     * @inheritdoc
     */
    public function getAlliancePublicInfo($allianceId)
    {
        foreach ($this->getAdapters() as $adapter)
        {
            try
            {
                return $adapter->getAlliancePublicInfo($allianceId);
            }
            catch (ApiResultException $apiResultException)
            {
                $this->logException($apiResultException);
            }
        }

        throw new ApiResultException('None of the adapters in chains were able to process the call.');
    }

    /**
     * @param ApiResultException $apiResultException
     */
    private function logException(ApiResultException $apiResultException)
    {
        $this->logger->notice(
            $apiResultException->getMessage(),
            [
                'api'        => $apiResultException->getApiName(),
                'parameters' => $apiResultException->getParameters()
            ]
        );
    }
}
