<?php

/*
 * This file is part of the Tz7\EveApiBundle package.
 *
 * (c) Adamus TorK <https://bitbucket.org/adamus/>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */


namespace Tz7\EveApiBundle\Model;


interface AllianceInterface extends ApiIdentifiedInterface, NamedEntityInterface, UpdatedEntityInterface
{
    /**
     * @return string
     */
    public function getShort();

    /**
     * @param string $short
     * @return $this
     */
    public function setShort($short);

    /**
     * @return CorporationInterface[]
     */
    public function getMembers();

    /**
     * @param CorporationInterface $corporation
     * @return $this
     */
    public function addMember(CorporationInterface $corporation);

    /**
     * @param CorporationInterface $corporation
     * @return $this
     */
    public function removeMember(CorporationInterface $corporation);
}
