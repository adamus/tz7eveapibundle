<?php

/*
 * This file is part of the Tz7\EveApiBundle package.
 *
 * (c) Adamus TorK <https://bitbucket.org/adamus/>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Tz7\EveApiBundle\Model;


interface CharacterInterface extends ApiIdentifiedInterface, NamedEntityInterface, UpdatedEntityInterface
{
    /**
     * @return CorporationInterface
     */
    public function getCorporation();

    /**
     * @param CorporationInterface $corporation
     * @return $this
     */
    public function setCorporation(CorporationInterface $corporation);
}
