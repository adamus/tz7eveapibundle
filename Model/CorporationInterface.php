<?php

/*
 * This file is part of the Tz7\EveApiBundle package.
 *
 * (c) Adamus TorK <https://bitbucket.org/adamus/>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Tz7\EveApiBundle\Model;


interface CorporationInterface extends ApiIdentifiedInterface, NamedEntityInterface, UpdatedEntityInterface
{
    /**
     * @return string
     */
    public function getTicker();

    /**
     * @param string $ticker
     * @return $this
     */
    public function setTicker($ticker);

    /**
     * @return CharacterInterface[]
     */
    public function getMembers();

    /**
     * @param CharacterInterface $character
     * @return $this
     */
    public function addMember(CharacterInterface $character);

    /**
     * @param CharacterInterface $character
     * @return $this
     */
    public function removeMember(CharacterInterface $character);

    /**
     * @return AllianceInterface
     */
    public function getAlliance();

    /**
     * @param AllianceInterface $alliance
     * @return $this
     */
    public function setAlliance(AllianceInterface $alliance = null);
}
