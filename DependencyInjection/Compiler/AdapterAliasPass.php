<?php

/*
 * This file is part of the Tz7\EveApiBundle package.
 *
 * (c) Adamus TorK <https://bitbucket.org/adamus/>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Tz7\EveApiBundle\DependencyInjection\Compiler;


use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;
use Symfony\Component\DependencyInjection\Reference;


/**
 * Bridging of adapters by the configuration.
 */
class AdapterAliasPass implements CompilerPassInterface
{
	/**
	 * @param ContainerBuilder $container
	 */
	public function process(ContainerBuilder $container)
	{
		foreach ($container->getParameter('tz7.eve_api.adapter_types') as $key)
		{
			$alias    = sprintf('tz7.eve_api.%s_adapter', $key);
			$adapters = $container->getParameter($alias);

			if (count($adapters) > 1)
			{
				// Bridge to chain of adapters.
				$chain      = sprintf('tz7.eve_api.%s_adapter_chain', $key);
				$definition = $container->getDefinition($chain);
				$container->setAlias($alias, $chain)->setPublic(true);

				foreach ($adapters as $adapter)
				{
					$definition->addMethodCall('addAdapter', [new Reference($adapter)]);
				}
			}
			else
			{
				// Bridge to a singe adapter.
				$container->setAlias($alias, $adapters[0])->setPublic(true);
			}
		}
	}
}
