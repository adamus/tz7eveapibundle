<?php


namespace Tz7\EveApiBundle\DependencyInjection;


use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;

use Tz7\EveApiBundle\Model\Api;
use Tz7\EveApiBundle\Model\CharacterInterface;
use Tz7\EveApiBundle\Model\CorporationInterface;
use Tz7\EveApiBundle\Model\AllianceInterface;
use Tz7\EveApiBundle\Model\Mail;
use Tz7\EveApiBundle\Model\Notification;


class Configuration implements ConfigurationInterface
{
    /**
     * {@inheritDoc}
     */
    public function getConfigTreeBuilder()
    {
        $treeBuilder = new TreeBuilder('tz7_eve_api');

        return $treeBuilder->getRootNode()
            ->children()
                ->scalarNode('user_agent')->isRequired()->cannotBeEmpty()->end()
                ->arrayNode('adapters')
                    ->addDefaultsIfNotSet()
                    ->children()
                        ->arrayNode('public')
                            ->beforeNormalization()
                            ->ifString()
                                ->then(function($v) { return [$v]; })
                            ->end()
                            ->prototype('variable')->end()
                            ->defaultValue(['tz7.eve_api.public_eve_swagger_adapter'])
                        ->end()
                    ->end()
                ->end()
                ->arrayNode('endpoints')
                    ->addDefaultsIfNotSet()
                    ->children()
                        ->scalarNode('public')->defaultNull()->end()
                    ->end()
                ->end()
                ->arrayNode('templates')
                    ->addDefaultsIfNotSet()
                    ->children()
                        ->arrayNode('crud')
                            ->addDefaultsIfNotSet()
                            ->children()
                                ->scalarNode('base_layout')->defaultValue('base.html.twig')->isRequired()->end()
                            ->end()
                        ->end()
                    ->end()
                ->end()
                ->arrayNode('model_map')
                    ->isRequired()
                    //->cannotBeEmpty()
                    ->append($this->createResourceNode('character', CharacterInterface::class, true))
                    ->append($this->createResourceNode('corporation', CorporationInterface::class, true))
                    ->append($this->createResourceNode('alliance', AllianceInterface::class, true))
                ->end()
            ->end()
        ->end();
    }

    private function createResourceNode($name, $interface, $isMandatory, $default = null)
    {
        $builder = new TreeBuilder($name, 'scalar');
        $node = $builder->getRootNode();

        if ($isMandatory) {
            $node
                ->isRequired()
                ->cannotBeEmpty()
            ;
        }

        if ($default) {
            $node->defaultValue($default);
        }

        $node
            ->validate()
                ->ifTrue(function ($v) use ($interface, $isMandatory, $default) {
                    return $default != $v && !$default && ((!$v && !$isMandatory) || !is_subclass_of($v, $interface));
                })
                ->thenInvalid(sprintf('must implement %s', $interface))
            ->end()
        ;

        return $node;
    }
}
