<?php

/*
 * This file is part of the Tz7\EveApiBundle package.
 *
 * (c) Adamus TorK <https://bitbucket.org/adamus/>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Tz7\EveApiBundle\DependencyInjection;


use Symfony\Component\Config\FileLocator;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Exception\InvalidArgumentException;
use Symfony\Component\DependencyInjection\Extension\PrependExtensionInterface;
use Symfony\Component\DependencyInjection\Loader;
use Symfony\Component\HttpKernel\DependencyInjection\Extension;
use Symfony\Component\Yaml\Yaml;
use Tz7\EveApiBundle\Model\AllianceInterface;
use Tz7\EveApiBundle\Model\CharacterInterface;
use Tz7\EveApiBundle\Model\CorporationInterface;


class Tz7EveApiExtension extends Extension implements PrependExtensionInterface
{
    /**
     * {@inheritDoc}
     */
    public function load(array $configs, ContainerBuilder $container)
    {
        $configuration = new Configuration();
        $config        = $this->processConfiguration($configuration, $configs);

        $container->setParameter('tz7.eve_api.user_agent', $config['user_agent']);

        $this->loadAdapterConfiguration($container, $config['adapters']);
        $this->loadEndpoints($container, $config['endpoints']);
        $this->setTemplateParameters($container, $config['templates']);

        $loader = new Loader\YamlFileLoader($container, new FileLocator(__DIR__ . '/../Resources/config'));
        $loader->load('services.yml');
    }

    /**
     * Allow an extension to prepend the extension configurations.
     *
     * @param ContainerBuilder $container
     */
    public function prepend(ContainerBuilder $container)
    {
        $configs = $container->getExtensionConfig($this->getAlias());
        $config  = $this->processConfiguration(new Configuration(), $configs);

        $modelMapping = $this->getModelMapFromConfig($config);
        $container->setParameter('tz7.eve_api.model_mapping', $modelMapping);

        $bundles = $container->getParameter('kernel.bundles');
        if (isset($bundles['DoctrineBundle']))
        {
            if ($container->hasExtension('doctrine'))
            {
                $container->prependExtensionConfig(
                    'doctrine',
                    [
                        'orm' => [
                            'resolve_target_entities' => $modelMapping
                        ]
                    ]
                );
            }
        }

        $container->prependExtensionConfig('monolog', ['channels' => ['eve_api']]);
    }

    /**
     * @param ContainerBuilder $container
     * @param array            $configuredAdapters
     */
    private function loadAdapterConfiguration(ContainerBuilder $container, array $configuredAdapters)
    {
        foreach ($configuredAdapters as $key => $adapters)
        {
            $container->setParameter(sprintf('tz7.eve_api.%s_adapter', $key), $adapters);
        }

        $container->setParameter('tz7.eve_api.adapter_types', array_keys($configuredAdapters));
    }

    /**
     * @param ContainerBuilder $container
     * @param array            $configuredEndpoints
     */
    private function loadEndpoints(ContainerBuilder $container, array $configuredEndpoints)
    {
        foreach ($configuredEndpoints as $key => $endpoint)
        {
            $name = sprintf('tz7.eve_api.%s_adapter_endpoint', $key);
            $container->setParameter($name, $endpoint);
        }
    }

    /**
     * @param array $config
     *
     * @return array
     */
    private function getModelMapFromConfig(array $config)
    {
        $mapping = [];

        $interfaceToKey = [
            CharacterInterface::class                      => 'character',
            CorporationInterface::class                    => 'corporation',
            AllianceInterface::class                       => 'alliance'
        ];

        foreach ($interfaceToKey as $interface => $key)
        {
            if (
                isset($config['model_map'][$key])
                && class_exists($config['model_map'][$key])
                && is_subclass_of($config['model_map'][$key], $interface)
            )
            {
                $mapping[$interface] = $config['model_map'][$key];
            }
        }

        return $mapping;
    }

    /**
     * @param ContainerBuilder $container
     * @param array            $templates
     */
    private function setTemplateParameters(ContainerBuilder $container, array $templates)
    {
        foreach ($templates as $scope => $children)
        {
            foreach ($children as $name => $value)
            {
                $container->setParameter(sprintf('tz7.eve_api.templates.%s.%s', $scope, $name), $value);
            }
        }
    }
}
