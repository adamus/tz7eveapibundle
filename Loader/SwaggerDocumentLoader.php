<?php

namespace Tz7\EveApiBundle\Loader;


use Symfony\Contracts\Cache\CacheInterface;
use Symfony\Contracts\Cache\ItemInterface;
use Tz7\EveSwaggerClient\Loader\DocumentLoader;
use Tz7\EveSwaggerClient\Validator\DocumentValidator;


class SwaggerDocumentLoader extends DocumentLoader
{
	/** @var CacheInterface */
	private $cache;

	/** @var int */
	private $cacheLifetime;

	/**
	 * @param DocumentValidator $documentValidator
	 * @param CacheInterface    $cache
	 * @param int               $cacheLifetime
	 */
	public function __construct(DocumentValidator $documentValidator, CacheInterface $cache, $cacheLifetime)
	{
		parent::__construct($documentValidator);

		$this->cache         = $cache;
		$this->cacheLifetime = $cacheLifetime;
	}

	/**
	 * @inheritdoc
	 */
	public function load($url)
	{
		$key = $this->generateCacheKey($url);

		return $this->cache->get(
			$key,
			function (ItemInterface $item) use ($url)
			{
				$item->expiresAfter($this->cacheLifetime);

				return parent::load($url);
			}
		);
	}

	/**
	 * @param string $url
	 *
	 * @return string
	 */
	protected function generateCacheKey($url)
	{
		return md5(__CLASS__ . '__' . $url);
	}
}
