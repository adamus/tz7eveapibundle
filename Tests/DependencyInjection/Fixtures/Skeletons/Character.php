<?php

/*
 * This file is part of the Tz7\EveApiBundle package.
 *
 * (c) Adamus TorK <https://bitbucket.org/adamus/>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Tz7\EveApiBundle\Tests\DependencyInjection\Fixtures\Skeletons;

use DateTimeInterface;
use Tz7\EveApiBundle\Model\CharacterInterface;
use Tz7\EveApiBundle\Model\CorporationInterface;

class Character implements CharacterInterface
{
    public function getId()
    {
    }

    public function setId($id)
    {
    }

    public function getCorporation()
    {
    }

    public function setCorporation(CorporationInterface $corporation)
    {
    }

    public function getName()
    {
    }

    public function setName($name)
    {
    }

    public function getUpdatedAt()
    {
    }

    public function setUpdatedAt(DateTimeInterface $updatedAt = null)
    {
    }
}
