<?php

/*
 * This file is part of the Tz7\EveApiBundle package.
 *
 * (c) Adamus TorK <https://bitbucket.org/adamus/>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Tz7\EveApiBundle\Tests\DependencyInjection\Fixtures\Skeletons;

use DateTimeInterface;
use Tz7\EveApiBundle\Model\AllianceInterface;
use Tz7\EveApiBundle\Model\CharacterInterface;
use Tz7\EveApiBundle\Model\CorporationInterface;


class Corporation implements CorporationInterface
{
    public function getId()
    {
    }

    public function setId($id)
    {
    }

    public function getTicker()
    {
    }

    public function setTicker($ticker)
    {
    }

    public function getMembers()
    {
    }

    public function addMember(CharacterInterface $character)
    {
    }

    public function removeMember(CharacterInterface $character)
    {
    }

    public function getAlliance()
    {
    }

    public function setAlliance(AllianceInterface $alliance = null)
    {
    }

    public function getName()
    {
    }

    public function setName($name)
    {
    }

    public function getUpdatedAt()
    {
    }

    public function setUpdatedAt(DateTimeInterface $updatedAt = null)
    {
    }
}
