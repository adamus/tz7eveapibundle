<?php

/*
 * This file is part of the Tz7\EveApiBundle package.
 *
 * (c) Adamus TorK <https://bitbucket.org/adamus/>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Tz7\EveApiBundle\Tests\DependencyInjection;


use Doctrine\ORM\EntityManager;
use PHPUnit\Framework\TestCase;
use Psr\Log\NullLogger;
use Symfony\Component\Cache\Adapter\NullAdapter;
use Symfony\Component\Config\FileLocator;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Loader\YamlFileLoader;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBag;
use Tz7\EveApiBundle\Adapter\PublicAdapterChain;
use Tz7\EveApiBundle\DependencyInjection\Compiler\AdapterAliasPass;
use Tz7\EveApiBundle\DependencyInjection\Tz7EveApiExtension;
use Tz7\EveApiBundle\Manager\AbstractManagerInterface;
use Symfony\Component\Routing\Router;
use Tz7\EveApiBundle\Service\PublicAPI;
use Tz7\EveApiClient\Adapter\EveSwaggerApi\PublicSwaggerAdapter;
use Tz7\EveApiClient\Adapter\PublicFunctionAdapterInterface;
use Tz7\EveApiClient\Model\ModelFactoryProvider;
use Tz7\EveSwaggerClient\ClientInterface;
use Tz7\EveSwaggerClient\Factory\ResourceFactory;


class ContainerConfigTest extends TestCase
{
    public function testValidConfig()
    {
        $container = $this->createBundleExtension();

        $this->loadConfig($container, '/Fixtures/config', 'valid_config.yml');
        $container->compile();

        $serviceMap = [
            'tz7.eve_api.model_factory_provider'                => ModelFactoryProvider::class,
            'tz7.eve_api.abstract_manager'                      => AbstractManagerInterface::class,
            'tz7.eve_swagger.client'                            => ClientInterface::class,
            'tz7.eve_swagger.factory.resource'                  => ResourceFactory::class,
            'tz7.eve_api.public_eve_swagger_adapter'            => PublicSwaggerAdapter::class,
            'tz7.eve_api.public_adapter'                        => PublicFunctionAdapterInterface::class,
            'tz7.eve_api.public'                                => PublicAPI::class,
        ];

        foreach ($serviceMap as $id => $interface)
        {
            $this->assertInstanceOf($interface, $container->get($id));
        }
    }

    public function testChainConfig()
    {
        $container = $this->createBundleExtension();
        $this->loadConfig($container, '/Fixtures/config', 'chain_config.yml');
        $container->compile();

        /** @var PublicAdapterChain $chain */
        $chain = $container->get('tz7.eve_api.public_adapter');

        $this->assertInstanceOf(PublicAdapterChain::class, $chain);
        $this->assertNotEmpty($chain->getAdapters());
        $this->assertInstanceOf(PublicSwaggerAdapter::class, $chain->getAdapters()[0]);
    }

    /**
     * @return ContainerBuilder
     */
    protected function createContainer()
    {
        return new ContainerBuilder(
            new ParameterBag(
                [
                    'kernel.debug'       => false,
                    'kernel.bundles'     => [],
                    'kernel.environment' => 'test',
                    'kernel.root_dir'    => __DIR__ . '/../',
                    'kernel.cache_dir'   => sys_get_temp_dir(),
                ]
            )
        );
    }

    /**
     * @return ContainerBuilder
     */
    protected function createBundleExtension()
    {
        $container = $this->createContainer();
        $loader    = new Tz7EveApiExtension();

        $container->registerExtension($loader);
        $container->addCompilerPass(new AdapterAliasPass());
        $this->mockDependencies($container);

        $this->loadConfig($container, '/../../Resources/config', 'services.yml');

        return $container;
    }

    /**
     * @param ContainerBuilder $container
     */
    protected function mockDependencies(ContainerBuilder $container)
    {
        $routerMock = $this
            ->getMockBuilder(Router::class)
            ->disableOriginalConstructor()
            ->getMock();

        $entityManagerMock = $this
            ->getMockBuilder(EntityManager::class)
            ->disableOriginalConstructor()
            ->getMock();

        $container->set('tz7.eve_swagger.cache', new NullAdapter());
        $container->set('router', $routerMock);
        $container->set('monolog.logger.eve_api', new NullLogger());
        $container->set('doctrine.orm.entity_manager', $entityManagerMock);
    }

    /**
     * @param ContainerBuilder $container
     * @param string           $dir
     * @param string           $file
     */
    protected function loadConfig(ContainerBuilder $container, $dir, $file)
    {
        $loader = new YamlFileLoader($container, new FileLocator(__DIR__ . $dir));
        $loader->load($file);
    }
}
