<?php

/*
 * This file is part of the Tz7\EveApiBundle package.
 *
 * (c) Adamus TorK <https://bitbucket.org/adamus/>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Tz7\EveApiBundle\Tests\Service;

use DateTime;
use DateTimeImmutable;
use InvalidArgumentException;
use PHPUnit\Framework\TestCase;
use PHPUnit_Framework_MockObject_MockObject;
use Psr\Log\NullLogger;
use Tz7\EveApiBundle\Manager\AbstractManager;
use Tz7\EveApiBundle\Mapper\DataMapperFactory;
use Tz7\EveApiBundle\Model\AllianceInterface;
use Tz7\EveApiBundle\Model\CharacterInterface;
use Tz7\EveApiBundle\Model\CorporationInterface;
use Tz7\EveApiBundle\Service\PublicAPI;
use Tz7\EveApiClient\Adapter\PublicFunctionAdapterInterface;
use Tz7\EveApiClient\Model\Alliance\AllianceInfo;
use Tz7\EveApiClient\Model\Character\CharacterInfo;
use Tz7\EveApiClient\Model\Corporation\CorporationSheet;


class PublicAPITestCase extends TestCase
{
    /** @var CharacterInfo */
    private $characterInfo;

    /** @var CorporationSheet */
    private $corporationSheet;

    /** @var AllianceInfo */
    private $allianceInfo;

    /**
     * @inheritdoc
     */
    protected function setUp()
    {
        parent::setUp();

        $this->characterInfo    = new CharacterInfo(
            999856849,
            'Adamus TorK',
            545096348,
            [],
            null
        );
        $this->corporationSheet = new CorporationSheet(
            545096348,
            'HUN Corp.',
            'HUN',
            1399057309,
            'RIP',
            [],
            null
        );
        $this->allianceInfo     = new AllianceInfo(
            1399057309,
            'HUN Reloaded',
            'RAX',
            545096348,
            new DateTimeImmutable('2007-04-16')
        );
    }

    /**
     * @dataProvider provideTestCases
     *
     * @param string    $expectedManagerMethod
     * @param DateTime $updatedAt
     * @param bool      $expectsAction
     */
    public function testGetCharacter($expectedManagerMethod, DateTime $updatedAt, $expectsAction)
    {
        $publicAdapterMock = $this->createPublicAdapterMock();
        $managerMock       = $this->createManagerMock($expectedManagerMethod, $updatedAt, $expectsAction);

        $this->addCharacterPublicInfo($publicAdapterMock, $expectsAction);
        $this->addCorporationPublicInfo($publicAdapterMock, $expectsAction);
        $this->addAlliancePublicInfo($publicAdapterMock, $expectsAction);

        $publicAPI = $this->createPublicAPI($publicAdapterMock, $managerMock);
        $publicAPI->getCharacter(999856849);
    }

    /**
     * @dataProvider provideTestCases
     *
     * @param string    $expectedManagerMethod
     * @param DateTime $updatedAt
     * @param bool      $expectsAction
     */
    public function testGetCorporation($expectedManagerMethod, DateTime $updatedAt, $expectsAction)
    {
        $publicAdapterMock = $this->createPublicAdapterMock();
        $managerMock       = $this->createManagerMock($expectedManagerMethod, $updatedAt, $expectsAction);

        $this->addCharacterPublicInfo($publicAdapterMock, false);
        $this->addCorporationPublicInfo($publicAdapterMock, $expectsAction);
        $this->addAlliancePublicInfo($publicAdapterMock, $expectsAction);

        $publicAPI = $this->createPublicAPI($publicAdapterMock, $managerMock);
        $publicAPI->getCorporation(545096348);
    }

    /**
     * @dataProvider provideTestCases
     *
     * @param string    $expectedManagerMethod
     * @param DateTime $updatedAt
     * @param bool      $expectsAction
     */
    public function testGetAlliance($expectedManagerMethod, DateTime $updatedAt, $expectsAction)
    {
        $publicAdapterMock = $this->createPublicAdapterMock();
        $managerMock       = $this->createManagerMock($expectedManagerMethod, $updatedAt, $expectsAction);

        $this->addCharacterPublicInfo($publicAdapterMock, false);
        $this->addCorporationPublicInfo($publicAdapterMock, false);
        $this->addAlliancePublicInfo($publicAdapterMock, $expectsAction);

        $publicAPI = $this->createPublicAPI($publicAdapterMock, $managerMock);
        $publicAPI->getAlliance(1399057309);
    }

    public function testUpdateCharacterPublic()
    {
        $updatedAt = new DateTime('now');

        $publicAdapterMock = $this->createPublicAdapterMock();
        $managerMock       = $this->createManagerMock('find', $updatedAt, false);
        $characterMock     = $this->getCharacterMock(999856849, true, $updatedAt);

        $this->addCharacterPublicInfo($publicAdapterMock, true);
        $this->addCorporationPublicInfo($publicAdapterMock, false);
        $this->addAlliancePublicInfo($publicAdapterMock, false);

        $publicAPI = $this->createPublicAPI($publicAdapterMock, $managerMock);
        $publicAPI->updateCharacterPublic($characterMock);
    }

    public function testUpdateCorporationPublic()
    {
        $updatedAt = new DateTime('now');

        $publicAdapterMock = $this->createPublicAdapterMock();
        $managerMock       = $this->createManagerMock('find', $updatedAt, false);
        $corporationMock   = $this->getCorporationMock(545096348, true, $updatedAt);

        $this->addCharacterPublicInfo($publicAdapterMock, false);
        $this->addCorporationPublicInfo($publicAdapterMock, true);
        $this->addAlliancePublicInfo($publicAdapterMock, false);

        $publicAPI = $this->createPublicAPI($publicAdapterMock, $managerMock);
        $publicAPI->updateCorporationPublic($corporationMock);
    }

    public function testUpdateAlliancePublic()
    {
        $updatedAt = new DateTime('now');

        $publicAdapterMock = $this->createPublicAdapterMock();
        $managerMock       = $this->createManagerMock('find', $updatedAt, false);
        $allianceMock      = $this->getAllianceMock(1399057309, true, $updatedAt);

        $this->addCharacterPublicInfo($publicAdapterMock, false);
        $this->addCorporationPublicInfo($publicAdapterMock, false);
        $this->addAlliancePublicInfo($publicAdapterMock, true);

        $publicAPI = $this->createPublicAPI($publicAdapterMock, $managerMock);
        $publicAPI->updateAlliancePublic($allianceMock);
    }

    /**
     * @return array
     */
    public function provideTestCases()
    {
        return [
            [
                'create',
                new DateTime('now'),
                true
            ],
            [
                'find',
                new DateTime('now'),
                false
            ],
            [
                'find',
                new DateTime('now -1 year'),
                true
            ]
        ];
    }

    /**
     * @param PublicFunctionAdapterInterface|PHPUnit_Framework_MockObject_MockObject $publicAdapterMock
     * @param AbstractManager|PHPUnit_Framework_MockObject_MockObject $managerMock
     *
     * @return PublicAPI
     */
    private function createPublicAPI($publicAdapterMock, $managerMock)
    {
        return new PublicAPI(
            $publicAdapterMock,
            $managerMock,
            new DataMapperFactory(),
            $this->getLoggerMock()
        );
    }

    /**
     * @return PublicFunctionAdapterInterface|PHPUnit_Framework_MockObject_MockObject
     */
    private function createPublicAdapterMock()
    {
        $publicAdapterMock = $this
            ->getMockBuilder(PublicFunctionAdapterInterface::class)
            ->setMethods([])
            ->getMock();

        return $publicAdapterMock;
    }

    /**
     * @param PHPUnit_Framework_MockObject_MockObject $publicAdapterMock
     * @param bool                                    $expectsAction
     */
    private function addCharacterPublicInfo(PHPUnit_Framework_MockObject_MockObject $publicAdapterMock, $expectsAction)
    {
        $publicAdapterMock
            ->expects($expectsAction ? $this->once() : $this->never())
            ->method('getCharacterPublicInfo')
            ->with($this->characterInfo->getCharacterID())
            ->willReturn($this->characterInfo);
    }

    /**
     * @param PHPUnit_Framework_MockObject_MockObject $publicAdapterMock
     * @param bool                                    $expectsAction
     */
    private function addCorporationPublicInfo(PHPUnit_Framework_MockObject_MockObject $publicAdapterMock, $expectsAction)
    {
        $publicAdapterMock
            ->expects($expectsAction ? $this->once() : $this->never())
            ->method('getCorporationPublicInfo')
            ->with($this->corporationSheet->getCorporationID())
            ->willReturn($this->corporationSheet);
    }

    /**
     * @param PHPUnit_Framework_MockObject_MockObject $publicAdapterMock
     * @param bool                                    $expectsAction
     */
    private function addAlliancePublicInfo(PHPUnit_Framework_MockObject_MockObject $publicAdapterMock, $expectsAction)
    {
        $publicAdapterMock
            ->expects($expectsAction ? $this->once() : $this->never())
            ->method('getAlliancePublicInfo')
            ->with($this->allianceInfo->getAllianceID())
            ->willReturn($this->allianceInfo);
    }

    /**
     * @param string    $expectedManagerMethod
     * @param DateTime $updatedAt
     * @param bool      $expectsAction
     *
     * @return AbstractManager|PHPUnit_Framework_MockObject_MockObject
     */
    private function createManagerMock($expectedManagerMethod, DateTime $updatedAt, $expectsAction)
    {
        $managerMock = $this
            ->getMockBuilder(AbstractManager::class)
            ->disableOriginalConstructor()
            ->setMethods([])
            ->getMock();

        $managerMock
            ->expects($expectedManagerMethod === 'create' ? $this->atLeastOnce() : $this->never())
            ->method('persist');

        $managerMock
            ->method($expectedManagerMethod)
            ->willReturnCallback(
                function ($class, $id) use ($expectsAction, $updatedAt)
                {
                    switch ($class)
                    {
                        case CharacterInterface::class:
                            return $this->getCharacterMock($id, $expectsAction, $updatedAt);

                        case CorporationInterface::class:
                            return $this->getCorporationMock($id, $expectsAction, $updatedAt);

                        case AllianceInterface::class:
                            return $this->getAllianceMock($id, $expectsAction, $updatedAt);

                        default:
                            throw new InvalidArgumentException('Invalid class ' . $class);
                    }
                }
            );

        return $managerMock;
    }

    /**
     * @return NullLogger|PHPUnit_Framework_MockObject_MockObject
     */
    private function getLoggerMock()
    {
        $loggerMock = $this
            ->getMockBuilder(NullLogger::class)
            ->setMethods([])
            ->getMock();

        return $loggerMock;
    }

    /**
     * @param int       $id
     * @param bool      $expectsAction
     * @param DateTime|$updatedAt
     *
     * @return CharacterInterface|PHPUnit_Framework_MockObject_MockObject
     */
    private function getCharacterMock($id, $expectsAction, $updatedAt)
    {
        $mock = $this
            ->getMockBuilder(CharacterInterface::class)
            ->setMethods([])
            ->getMock();

        $mock
            ->expects($expectsAction ? $this->once() : $this->never())
            ->method('setName')
            ->with($this->characterInfo->getCharacterName());

        $mock
            ->expects($expectsAction ? $this->once() : $this->never())
            ->method('setUpdatedAt');

        $mock
            ->method('getId')
            ->willReturn($id);

        $mock
            ->method('getUpdatedAt')
            ->willReturn($updatedAt);

        return $mock;
    }

    /**
     * @param int       $id
     * @param bool      $expectsAction
     * @param DateTime|$updatedAt
     *
     * @return CorporationInterface|PHPUnit_Framework_MockObject_MockObject
     */
    private function getCorporationMock($id, $expectsAction, $updatedAt)
    {
        $mock = $this
            ->getMockBuilder(CorporationInterface::class)
            ->setMethods([])
            ->getMock();

        $mock
            ->expects($expectsAction ? $this->once() : $this->never())
            ->method('setName')
            ->with($this->corporationSheet->getCorporationName());

        $mock
            ->expects($expectsAction ? $this->once() : $this->never())
            ->method('setTicker')
            ->with($this->corporationSheet->getTicker());

        $mock
            ->expects($expectsAction ? $this->once() : $this->never())
            ->method('setAlliance');

        $mock
            ->method('getId')
            ->willReturn($id);

        $mock
            ->method('getUpdatedAt')
            ->willReturn($updatedAt);

        return $mock;
    }

    /**
     * @param int       $id
     * @param bool      $expectsAction
     * @param DateTime|$updatedAt
     *
     * @return AllianceInterface|PHPUnit_Framework_MockObject_MockObject
     */
    private function getAllianceMock($id, $expectsAction, $updatedAt)
    {
        $mock = $this
            ->getMockBuilder(AllianceInterface::class)
            ->setMethods([])
            ->getMock();

        $mock
            ->expects($expectsAction ? $this->once() : $this->never())
            ->method('setName')
            ->with($this->allianceInfo->getName());

        $mock
            ->expects($expectsAction ? $this->once() : $this->never())
            ->method('setShort')
            ->with($this->allianceInfo->getShortName());

        $mock
            ->expects($expectsAction ? $this->once() : $this->never())
            ->method('setUpdatedAt');

        $mock
            ->method('getId')
            ->willReturn($id);

        $mock
            ->method('getUpdatedAt')
            ->willReturn($updatedAt);

        return $mock;
    }
}
