<?php

namespace Tz7\EveApiBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Tz7\EveApiBundle\DependencyInjection\Compiler\AdapterAliasPass;


class Tz7EveApiBundle extends Bundle
{
    public function build(ContainerBuilder $container)
    {
        $container->addCompilerPass(new AdapterAliasPass());
    }
}
