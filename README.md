TZ-7: EveApiBundle
======

This Symfony 2 bundle is part of the [TZ-7 project](https://bitbucket.org/adamus/tz-7), 
providing object-oriented layer to 
[EVE Online API](https://eveonline-third-party-documentation.readthedocs.io/en/latest/xmlapi/index.html).

[Documentation](Resources/doc/index.md)
