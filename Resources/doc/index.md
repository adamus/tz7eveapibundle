TZ-7: EveApiBundle
======

 
### The Client ###

Although the bundle is mainly using PhealNG to communicate with the EVE API, it is also capable to use the 
new ESI (EVE Swagger Interface) by providing a common interface with [TZ-7\EveApiClient](https://bitbucket.org/adamus/tz7eveapiclient).
 

## Installation ##

    composer require tz7/eve-api-bundle:dev-master


Implement the following interfaces:

* [AllianceInterface](../../Model/AllianceInterface.php)
* [CharacterInterface](../../Model/CharacterInterface.php)
* [CorporationInterface](../../Model/CorporationInterface.php)

Bundle needs to be registered in the AppKernel:

    new Tz7\EveApiBundle\Tz7EveApiBundle(),


## Configuration ##
    tz7_eve_api:
      user_agent: your contact to CCP <if.things@goes.wrong> as API docs defines
      adapters:
        # You can define your own services and pass their id(s)
        character: 'tz7.eve_api.public_eve_swagger_adapter' # Singe adapter
        public: # Chain of adapters
          - 'tz7.eve_api.public_eve_swagger_adapter'
          - 'tz7.eve_api.public_eve_foo_adapter'
      model_map:
        character: YourBundle\Entity\YourCharacter # implementing Tz7\EveApiBundle\Model\CharacterInterface
        corporation: YourBundle\Entity\YourCorporation # implementing Tz7\EveApiBundle\Model\CorporationInterface
        alliance: YourBundle\Entity\YourAlliance # implementing Tz7\EveApiBundle\Model\AllianceInterface
