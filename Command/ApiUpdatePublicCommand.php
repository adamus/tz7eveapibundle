<?php

/*
 * This file is part of the Tz7\EveApiBundle package.
 *
 * (c) Adamus TorK <https://bitbucket.org/adamus/>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Tz7\EveApiBundle\Command;


use DateTime;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

use Tz7\EveApiBundle\Model\AllianceInterface;
use Tz7\EveApiBundle\Model\CharacterInterface;
use Tz7\EveApiBundle\Model\CorporationInterface;
use Tz7\EveApiBundle\Service\PublicAPI;


class ApiUpdatePublicCommand extends Command
{
	/** @var EntityManagerInterface */
	private $entityManager;

	/** @var PublicAPI */
	private $publicAPI;

	public function __construct(EntityManagerInterface $entityManager, PublicAPI $publicAPI)
	{
		parent::__construct();

		$this->entityManager = $entityManager;
		$this->publicAPI     = $publicAPI;
	}

	protected function configure()
	{
		$this
			->setName('tz7:eve-api:update-public')
			->setDescription('Update public API data')
			->addOption('ttl', null, InputOption::VALUE_OPTIONAL, "Time To Live", "1 day")
			->addOption('sleep', null, InputOption::VALUE_OPTIONAL, "Wait between calls.", 1)
			->addOption('limit', null, InputOption::VALUE_OPTIONAL, "Entities per type are updated in one call", 20);
	}

	/**
	 * @param InputInterface  $input
	 * @param OutputInterface $output
	 *
	 * @return int
	 */
	protected function execute(InputInterface $input, OutputInterface $output)
	{
		$ttl   = (new DateTime('now'))->modify(sprintf('-%s', $input->getOption('ttl')));
		$sleep = $input->getOption('sleep');
		$limit = $input->getOption('limit');

		$this->updateCharacters($ttl, $sleep, $limit, $output);
		$this->updateCorporations($ttl, $sleep, $limit, $output);
		$this->updateAlliances($ttl, $sleep, $limit, $output);

		return 0;
	}

	/**
	 * @param DateTime        $ttl
	 * @param int             $sleep
	 * @param int             $limit
	 * @param OutputInterface $output
	 */
	protected function updateCharacters($ttl, $sleep, $limit, OutputInterface $output)
	{
		$output->writeln(sprintf('<info>Updating %d characters</info>', $limit));

		/** @var CharacterInterface[] $characters */
		$characters = $this->entityManager->createQueryBuilder()
			->select('c')->from(CharacterInterface::class, 'c')
			->where('c.updatedAt <= :ttl')->setParameter('ttl', $ttl)
			->orderBy('c.updatedAt', 'ASC')
			->setMaxResults($limit)
			->getQuery()->getResult();

		foreach ($characters as $character)
		{
			try
			{
				$this->publicAPI->updateCharacterPublic($character);
				$this->entityManager->flush();
			}
			catch (\Exception $ex)
			{
				$output->writeln($ex->getMessage());
			}
			sleep($sleep);
		}
	}

	/**
	 * @param DateTime        $ttl
	 * @param int             $sleep
	 * @param int             $limit
	 * @param OutputInterface $output
	 */
	protected function updateCorporations($ttl, $sleep, $limit, OutputInterface $output)
	{
		$output->writeln(sprintf('<info>Updating %d corporations</info>', $limit));

		/** @var CorporationInterface[] $corporations */
		$corporations = $this->entityManager->createQueryBuilder()
			->select('c')->from(CorporationInterface::class, 'c')
			->where('c.updatedAt <= :ttl')->setParameter('ttl', $ttl)
			->orderBy('c.updatedAt', 'ASC')
			->setMaxResults($limit)
			->getQuery()->getResult();

		foreach ($corporations as $corporation)
		{
			try
			{
				$this->publicAPI->updateCorporationPublic($corporation);
				$this->entityManager->flush();
			}
			catch (\Exception $ex)
			{
				$output->writeln($ex->getMessage());
			}
			sleep($sleep);
		}
	}

	/**
	 * @param DateTime        $ttl
	 * @param int             $sleep
	 * @param int             $limit
	 * @param OutputInterface $output
	 */
	protected function updateAlliances($ttl, $sleep, $limit, OutputInterface $output)
	{
		$output->writeln(sprintf('<info>Updating %d alliances</info>', $limit));

		/** @var AllianceInterface[] $alliances */
		$alliances = $this->entityManager->createQueryBuilder()
			->select('a')->from(AllianceInterface::class, 'a')
			->where('a.updatedAt <= :ttl')->setParameter('ttl', $ttl)
			->orderBy('a.updatedAt', 'ASC')
			->setMaxResults($limit)
			->getQuery()->getResult();

		foreach ($alliances as $alliance)
		{
			try
			{
				$this->publicAPI->updateAlliancePublic($alliance);
				$this->entityManager->flush();
			}
			catch (\Exception $ex)
			{
				$output->writeln($ex->getMessage());
			}
			sleep($sleep);
		}
	}
}
