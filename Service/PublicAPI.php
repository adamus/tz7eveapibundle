<?php

/*
 * This file is part of the Tz7\EveApiBundle package.
 *
 * (c) Adamus TorK <https://bitbucket.org/adamus/>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Tz7\EveApiBundle\Service;


use DateInterval;
use DateTime;
use Psr\Log\LoggerInterface;
use Tz7\EveApiBundle\Manager\AbstractManagerInterface;
use Tz7\EveApiBundle\Mapper\DataMapperFactoryInterface;
use Tz7\EveApiBundle\Model\AllianceInterface;
use Tz7\EveApiBundle\Model\CharacterInterface;
use Tz7\EveApiBundle\Model\CorporationInterface;
use Tz7\EveApiBundle\Model\UpdatedEntityInterface;
use Tz7\EveApiClient\Adapter\PublicFunctionAdapterInterface;
use Tz7\EveApiClient\Exception\ApiResultException;


// @TODO Rename and move to be a Repository
// @TODO OnDemand update should be optional
class PublicAPI
{
    /** @var PublicFunctionAdapterInterface */
    protected $publicFunctionAdapter;

    /** @var AbstractManagerInterface */
    protected $manager;

    /** @var DataMapperFactoryInterface */
    protected $dataMapperFactory;

    /** @var LoggerInterface */
    protected $logger;

    /**
     * @param PublicFunctionAdapterInterface $publicFunctionAdapter
     * @param AbstractManagerInterface       $abstractManager
     * @param DataMapperFactoryInterface     $dataMapperFactory
     * @param LoggerInterface                $logger
     */
    public function __construct(
        PublicFunctionAdapterInterface $publicFunctionAdapter,
        AbstractManagerInterface $abstractManager,
        DataMapperFactoryInterface $dataMapperFactory,
        LoggerInterface $logger
    ) {
        $this->publicFunctionAdapter = $publicFunctionAdapter;
        $this->manager               = $abstractManager;
        $this->dataMapperFactory     = $dataMapperFactory;
        $this->logger                = $logger;

        // @TODO config
        $this->onDemandCharacterUpdate   = '2 day';
        $this->onDemandCorporationUpdate = '14 day';
        $this->onDemandAllianceUpdate    = '30 day';
    }

    /**
     * @param CharacterInterface $character
     */
    public function updateCharacterPublic(CharacterInterface $character)
    {
        $this->logger->debug(sprintf('%s: #%d', __METHOD__, $character->getId()));

        $characterInfo = $this->publicFunctionAdapter->getCharacterPublicInfo($character->getId());

        $this
            ->dataMapperFactory
            ->createCharacterDataMapper()
            ->mapCharacterInfo($this, $characterInfo, $character);

        $corporation = $character->getCorporation();
        if (!$corporation || $corporation->getId() != $characterInfo->getCorporationID())
        {
            $character->setCorporation($this->getCorporation($characterInfo->getCorporationID()));
        }
    }

    /**
     * @param CorporationInterface $corporation
     */
    public function updateCorporationPublic(CorporationInterface $corporation)
    {
        $this->logger->debug(sprintf('%s: #%d', __METHOD__, $corporation->getId()));

        $corporationSheet = $this->publicFunctionAdapter->getCorporationPublicInfo($corporation->getId());
        $this->updateCorporationInAlliance($corporation, $corporationSheet->getAllianceID());

        $this
            ->dataMapperFactory
            ->createCorporationDataMapper()
            ->mapCorporationSheet($this, $corporationSheet, $corporation);

        // @TODO update CEO
    }

    /**
     * @param AllianceInterface $alliance
     *
     * @throws \Exception
     */
    public function updateAlliancePublic(AllianceInterface $alliance)
    {
        $this->logger->debug(sprintf('%s: #%d', __METHOD__, $alliance->getId()));
        $allianceInfo = $this->publicFunctionAdapter->getAlliancePublicInfo($alliance->getId());

        if (!$allianceInfo)
        {
            throw new \Exception(sprintf('Unable to fetch Alliance #%d public Info', $alliance->getId()));
        }

        $this
            ->dataMapperFactory
            ->createAllianceDataMapper()
            ->mapAllianceInfo($this, $allianceInfo, $alliance);
    }

    /**
     * @param CorporationInterface $corporation
     * @param int                  $allianceID
     */
    protected function updateCorporationInAlliance(CorporationInterface $corporation, $allianceID)
    {
        $this->logger->debug(sprintf('%s: #%d in #%d', __METHOD__, $corporation->getId(), $allianceID));

        if (!$allianceID || $allianceID == "0")
        {
            $corporation->setAlliance(null);

            return;
        }

        $alliance = $corporation->getAlliance();
        if (!$alliance || $alliance->getId() != $allianceID)
        {
            $corporation->setAlliance($this->getAlliance($allianceID));
        }
    }

    /**
     * @param int $characterID
     *
     * @return CharacterInterface
     */
    public function getCharacter($characterID)
    {
        $this->logger->debug(sprintf('%s: #%d', __METHOD__, $characterID));
        $character = $this->manager->find(CharacterInterface::class, $characterID);

        if (!($character instanceof CharacterInterface))
        {
            $this->logger->info(sprintf('%s: Character not found, creating', __METHOD__));

            $character = $this->manager->create(CharacterInterface::class, $characterID);
            $this->updateCharacterPublic($character);
            $this->manager->persist($character);
        }
        elseif ($this->shouldUpdate($character))
        {
            try
            {
                $this->logger->info(sprintf('%s: Updating Character', __METHOD__));

                $this->updateCharacterPublic($character);
            }
            catch (ApiResultException $ex)
            {
                $this->logger->error(sprintf('%s: "%s"', __METHOD__, $ex->getMessage()));
            }
        }

        return $character;
    }

    /**
     * @param int $corporationID
     *
     * @return CorporationInterface
     */
    public function getCorporation($corporationID)
    {
        $this->logger->debug(sprintf('%s: #%d', __METHOD__, $corporationID));
        $corporation = $this->manager->find(CorporationInterface::class, $corporationID);

        if (!($corporation instanceof CorporationInterface))
        {
            $this->logger->info(sprintf('%s: Corporation not found, creating', __METHOD__));

            /** @var CorporationInterface $corporation */
            $corporation = $this->manager->create(CorporationInterface::class, $corporationID);
            $this->updateCorporationPublic($corporation);
            $this->manager->persist($corporation);
        }
        elseif ($this->shouldUpdate($corporation))
        {
            try
            {
                $this->logger->info(sprintf('%s: Updating Corporation', __METHOD__));

                $this->updateCorporationPublic($corporation);
            }
            catch (ApiResultException $ex)
            {
                $this->logger->error(sprintf('%s: "%s"', __METHOD__, $ex->getMessage()));
            }
        }

        return $corporation;
    }

    /**
     * @param int $allianceID
     *
     * @return AllianceInterface
     */
    public function getAlliance($allianceID)
    {
        $this->logger->debug(sprintf('%s: #%d', __METHOD__, $allianceID));
        $alliance = $this->manager->find(AllianceInterface::class, $allianceID);

        if (!($alliance instanceof AllianceInterface))
        {
            try
            {
                $this->logger->info(sprintf('%s: Alliance not found, creating', __METHOD__));

                /** @var AllianceInterface $alliance */
                $alliance = $this->manager->create(AllianceInterface::class, $allianceID);

                $this->updateAlliancePublic($alliance);
                $this->manager->persist($alliance);
            }
            catch (ApiResultException $ex)
            {
                $this->logger->notice(sprintf('%s: "%s"', __METHOD__, $ex->getMessage()));

                return null;
            }
        }
        elseif ($this->shouldUpdate($alliance))
        {
            try
            {
                $this->logger->info(sprintf('%s: Updating Alliance', __METHOD__));

                $this->updateAlliancePublic($alliance);
            }
            catch (ApiResultException $ex)
            {
                $this->logger->error(sprintf('%s: "%s"', __METHOD__, $ex->getMessage()));
            }
        }

        return $alliance;
    }

    /**
     * @param UpdatedEntityInterface $entity
     *
     * @return bool
     */
    private function shouldUpdate(UpdatedEntityInterface $entity)
    {
        $updatedAt = $entity->getUpdatedAt();

        if (!$updatedAt)
        {
            return true;
        }

        $now      = new DateTime();
        $updateAt = DateTime::createFromFormat('U', $updatedAt->getTimestamp());

        if ($entity instanceof CharacterInterface)
        {
            $updateAt->add(DateInterval::createFromDateString($this->onDemandCharacterUpdate));
        }
        elseif ($entity instanceof CorporationInterface)
        {
            $updateAt->add(DateInterval::createFromDateString($this->onDemandCorporationUpdate));
        }
        else
        {
            $updateAt->add(DateInterval::createFromDateString($this->onDemandAllianceUpdate));
        }

        return $now >= $updateAt;
    }
}
