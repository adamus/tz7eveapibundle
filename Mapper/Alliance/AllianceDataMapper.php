<?php

/*
 * This file is part of the Tz7\EveApiBundle package.
 *
 * (c) Adamus TorK <https://bitbucket.org/adamus/>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Tz7\EveApiBundle\Mapper\Alliance;


use Tz7\EveApiBundle\Model\AllianceInterface;
use Tz7\EveApiBundle\Service\PublicAPI;
use Tz7\EveApiClient\Model\Alliance\AllianceInfo;


class AllianceDataMapper implements AllianceDataMapperInterface
{
    /**
     * @inheritdoc
     */
    public function mapAllianceInfo(
        PublicAPI $publicAPI,
        AllianceInfo $allianceInfo,
        AllianceInterface $alliance
    ) {
        $alliance->setName($allianceInfo->getName());
        $alliance->setShort($allianceInfo->getShortName());
        $alliance->setUpdatedAt(new \DateTime);
    }
}
