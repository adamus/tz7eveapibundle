<?php

/*
 * This file is part of the Tz7\EveApiBundle package.
 *
 * (c) Adamus TorK <https://bitbucket.org/adamus/>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Tz7\EveApiBundle\Mapper\Corporation;


use Tz7\EveApiBundle\Model\CorporationInterface;
use Tz7\EveApiBundle\Service\PublicAPI;
use Tz7\EveApiClient\Model\Corporation\CorporationSheet;


interface CorporationDataMapperInterface
{
    /**
     * @param PublicAPI            $publicAPI
     * @param CorporationSheet     $corporationSheet
     * @param CorporationInterface $corporation
     */
    public function mapCorporationSheet(
        PublicAPI $publicAPI,
        CorporationSheet $corporationSheet,
        CorporationInterface $corporation
    );
}
