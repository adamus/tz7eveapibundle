<?php

/*
 * This file is part of the Tz7\EveApiBundle package.
 *
 * (c) Adamus TorK <https://bitbucket.org/adamus/>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Tz7\EveApiBundle\Mapper;


use Tz7\EveApiBundle\Mapper\Alliance\AllianceDataMapper;
use Tz7\EveApiBundle\Mapper\Character\CharacterDataMapper;
use Tz7\EveApiBundle\Mapper\Corporation\CorporationDataMapper;


// @TODO Split this
class DataMapperFactory implements DataMapperFactoryInterface
{
    /**
     * @inheritdoc
     */
    public function createCharacterDataMapper()
    {
        return new CharacterDataMapper();
    }

    /**
     * @inheritdoc
     */
    public function createCorporationDataMapper()
    {
        return new CorporationDataMapper();
    }

    /**
     * @inheritdoc
     */
    public function createAllianceDataMapper()
    {
        return new AllianceDataMapper();
    }
}
