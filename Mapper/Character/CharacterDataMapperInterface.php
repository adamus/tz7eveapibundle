<?php

/*
 * This file is part of the Tz7\EveApiBundle package.
 *
 * (c) Adamus TorK <https://bitbucket.org/adamus/>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Tz7\EveApiBundle\Mapper\Character;


use Tz7\EveApiBundle\Model\CharacterInterface;
use Tz7\EveApiBundle\Service\PublicAPI;
use Tz7\EveApiClient\Model\Character\CharacterInfo;


interface CharacterDataMapperInterface
{
    /**
     * @param PublicAPI          $publicAPI
     * @param CharacterInfo      $characterInfo
     * @param CharacterInterface $character
     */
    public function mapCharacterInfo(
        PublicAPI $publicAPI,
        CharacterInfo $characterInfo,
        CharacterInterface $character
    );
}
