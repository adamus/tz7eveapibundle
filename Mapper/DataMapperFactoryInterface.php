<?php

/*
 * This file is part of the Tz7\EveApiBundle package.
 *
 * (c) Adamus TorK <https://bitbucket.org/adamus/>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Tz7\EveApiBundle\Mapper;


use Tz7\EveApiBundle\Mapper\Alliance\AllianceDataMapperInterface;
use Tz7\EveApiBundle\Mapper\Character\CharacterDataMapperInterface;
use Tz7\EveApiBundle\Mapper\Corporation\CorporationDataMapperInterface;


interface DataMapperFactoryInterface
{
    /**
     * @return CharacterDataMapperInterface
     */
    public function createCharacterDataMapper();

    /**
     * @return CorporationDataMapperInterface
     */
    public function createCorporationDataMapper();

    /**
     * @return AllianceDataMapperInterface
     */
    public function createAllianceDataMapper();
}
